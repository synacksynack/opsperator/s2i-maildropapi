# k8s Maildrop API

Maildrop API

Forked from gitlab.com/markbeeson/maildrop/

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name      |    Description                       | Default          |
| :-------------------- | ------------------------------------ | ---------------- |
|  `ALTINBOX_MOD`       | m242/Maildrop Alt Inbox ID           | `20190422`       |
|  `BIND_PORT`          | m242/Maildrop API port               | `8080`           |
|  `DO_PROMETHEUS`      | m242/Maildrop API metrics            | undef            |
|  `REDIS`              | m242/Maildrop redis backend          | `127.0.0.1:6379` |
