'use strict';
const express = require('express');
const mb = require('./mailbox.js');
const st = require('./statistics.js');

const app = express();
const port = parseInt(process.env.BIND_PORT || 8080);

function getHost(req) {
    return {
	    from: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
	    to: req.headers.host
	};
}

if (process.env.AIRBRAKE_ID !== undefined && process.env.AIRBRAKE_ID !== ""
	&& process.env.AIRBRAKE_KEY !== undefined && process.env.AIRBRAKE_KEY !== "") {
    try {
	let airbrake = require('airbrake').createClient(process.env.AIRBRAKE_ID, process.env.AIRBRAKE_KEY);
	airbrake.handleExceptions();
	if (process.env.AIRBRAKE_HOST !== undefined) {
	    console.log(`exceptions would be sent to ${process.env['AIRBRAKE_HOST']}`);
	} else { console.log(`exceptions would be sent to airbrake.io`); }
    } catch(e) {
	console.log(`failed configuring airbrake, caught:`);
	console.log(e);
    }
} else { console.log(`airbrake disabled`); }

if (process.env.DO_PROMETHEUS !== undefined && process.env.DO_PROMETHEUS !== "") {
    const promBundle = require("express-prom-bundle");
    const metricsMiddleware = promBundle({
	    buckets: [0.1, 0.5, 1, 1.5],
	    includeMethod: true,
	    includePath: true,
	    includeStatusCode: true,
	    metricsPath: '/metrics'
	});

    app.use(metricsMiddleware);
}

app.use((req, res, next) => {
	res.append('Access-Control-Allow-Origin', '*');
	res.append('Access-Control-Allow-Credentials','true');
	res.append('Access-Control-Allow-Headers', '*');
	res.append('Access-Control-Allow-Methods', 'GET,DELETE');
	next();
    });

app.get('/', (req, res) => {
	let q = getHost(req);
	console.log(`${q.from} GET api index`);
	res.send('pong');
    });

app.delete('/mailbox/:mailbox/:message', (req, res) => {
	let q = getHost(req);
	console.log(`${q.from} DELETE ${req.params.mailbox}/${req.params.message} message`);
	mb.deleteBoth(req.params.mailbox, req.params.message)
	    .then((ok) => res.send(JSON.stringify({ deleted: ok })))
	    .catch((e) => console.error(e));
    });

app.get('/mailbox/:mailbox/:message', (req, res) => {
	let q = getHost(req);
	console.log(`${q.from} GET ${req.params.mailbox}/${req.params.message} message`);
	mb.getMessage(req.params.mailbox, req.params.message)
	    .then((ok) => mb.parseHtml(ok))
	    .then((ok) => res.send(JSON.stringify(ok)))
	    .catch((e) => console.error(e));
    });

app.get('/mailbox/:mailbox', (req, res) => {
	let q = getHost(req);
	console.log(`${q.from} GET ${req.params.mailbox} inbox`);
	mb.getInbox(req.params.mailbox)
	    .then((ok) => res.send(JSON.stringify({
					altinbox: mb.encryptMailbox(req.params.mailbox),
					messages: ok
		})))
	    .catch((e) => console.error(e));
    });

app.get('/statistics', (req, res) => {
	let q = getHost(req);
	console.log(`${q.from} GET statistics`);
	Promise.all([st.getStats("queued"), st.getStats("denied")])
	    .then((ok) => res.send(JSON.stringify({ "queued": ok[0], "denied": ok[1] })))
	    .catch((e) => console.error(e));
    });

app.listen(port, '0.0.0.0', () => {
	console.log(`Maildrop API listening on http:///:${port}`);
    });
