IMAGE = opsperator/maildropapi

.PHONY: install
install:
	@@NODE_ENV=build npm install --unsafe-perm
	@@npm install -g typescript

.PHONY: prep-runtime
prep-runtime:
	/.npm-global/bin/tsc
	cat index.js >dist/index.js

.PHONY: run
run: prep-runtime
	( cd dist/ ; node ./index.js )

.PHONY: run-pm2
run-pm2: prep-runtime
	@@if test -s /var/run/secrets/kubernetes.io/serviceaccount/token; then \
	    pm2 start ecosystem.config.js --no-daemon --no-vizion; \
	else \
	    pm2 start ecosystem.config.js --no-vizion; \
	fi

.PHONY: start
start: run

.PHONY: build
	docker build -t $(IMAGE) -f Dockerfile.debug-s2i .

.PHONY: dockertest
dockertest: build
	docker run -p8080:8080 -e REDIS=10.42.42.127:6379 \
	    --entrypoint /usr/libexec/s2i/run -it $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service configmap statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: test
test:
	@@echo no tests
